import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Route } from '@angular/router';
import { Account } from '../model/Account';
import {environement} from "../environements/environement";

@Injectable({
  providedIn: 'root'
})
export class UsrService {
  apiUrl = environement.apiUrl + "/demo"

  constructor(private http: HttpClient) { }

  getUser(){
    return this.http.get<Account[]>(this.apiUrl)
  }
}
