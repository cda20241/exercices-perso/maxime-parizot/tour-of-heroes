import { Account, AccountImpl } from './../model/Account';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Route, Router } from '@angular/router';
import { Observable } from 'rxjs';
import {environement} from "../environements/environement";

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  apiUrl = environement.apiUrl + "/api/v1/auth"

  account: Account = new AccountImpl()

  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  login(account: Account): Observable<Account>{
    return this.http.post<Account>(this.apiUrl + '/authenticate', account)
  }

  register(account: Account): Observable<Account>{
    return this.http.post<Account>(this.apiUrl + '/register', account)
  }

  accountLoggedSuccessfully(account: Account){
    this.account = account
    this.router.navigate(['/dashboard'])
  }

  isLogged(): boolean{
    console.log(this.account.token)
    const token = this.account.token
    return !! token
  }
}
