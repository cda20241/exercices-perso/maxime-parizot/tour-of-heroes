import { AccountService } from './account.service';
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";

@Injectable({
    providedIn: 'root'
})
export class AuthGuard {

  constructor(
      private router: Router,
      private accountService: AccountService,
  ){}

  canActivate(){
      return this.checkUserLogin();
    }

  checkUserLogin(): boolean{
    if(this.accountService.isLogged()){
      return true
    }
    this.router.navigate(['/login'])
    return false
  }

}
