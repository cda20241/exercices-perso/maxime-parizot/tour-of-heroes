import { Component } from '@angular/core';
import { Account, AccountImpl } from '../model/Account';
import { HttpClient } from '@angular/common/http';
import { AccountService } from '../services/account.service';
import {debounceTime} from "rxjs/operators";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  /** le compte de l'enseignant */

  /** Le constructeur */
  constructor(private http: HttpClient,
    private accountService: AccountService,
    ) {}

  /**
   * Au chargement du composant on supprime le token du precedant compte
   */
  ngOnInit(): void {
  }

  /**
   * Effectue une tentative d'inscription puis de connexion en utilisant le service de compte.
   */
  register(email: string, password: string) {
    console.log(email)
    let account = {} as Account
    account.email = email
    account.password = password
    console.log(account)
    this.accountService.register(account).subscribe(
      (response: any) => {
        console.log('Registration successful', response);
        this.accountService.login(account).subscribe(
          (account: Account) => {
            this.accountService.accountLoggedSuccessfully(account)
          },
        );
      },
    );
  }

}

