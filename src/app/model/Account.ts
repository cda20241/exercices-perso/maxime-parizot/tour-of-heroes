export interface Account{
  id: number
  email: string;
  password: string;
  token: string;
}

export class AccountImpl implements Account{
  id: number;
  email: string;
  password: string;
  token: string;

  constructor(account?: Account){
    this.id = account?.id || 0
    this.email = account?.email || "";
    this.password = account?.password || "";
    this.token = account?.token || "";
  }
}
